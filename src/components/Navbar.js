import React, {useContext} from 'react';
import { Link } from "react-router-dom";
import '../App.css';
import { Button } from '@material-ui/core';
import ThemeContext from '../context/ThemeContext';

function Navbar() {

  const {toggleTheme} = useContext(ThemeContext);

  return (
    <> 
      <div id="header" className="container header-container">          
             <nav className="navbar navbar-expand-lg navbar-light" style={{backgroundColor: 'white'}}>
              <div className="d-flex flex-wrap">
                <a href="/" className="navbar-brand">
                  Thohari Akbar
                </a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarScroll">
                  <ul className="navbar-nav" style={{scrollHeight: '200px'}}>
                    <li className="nav-item">
                      <a href="/" className="nav-link" aria-current="page">Home</a>
                    </li>
                    <li className="nav-item">
                      <a href="/portfolios" className="nav-link">Portfolios</a>
                    </li>
                    {/* <li className="nav-item">
                      <a href="/contactMe" className="nav-link">Contact</a>
                    </li> */}
                  </ul>
                  <div className="dark-mode">
                    <button onClick={toggleTheme} className='btn btn-dark switchBtn'>🌞🌜</button>
                  </div>
                </div>
              </div>
              </nav>
        </div>

    </>
  );
}

export default Navbar;
