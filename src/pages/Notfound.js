import React from 'react';
import Footer from '../components/Footer';
import Navbar from "../components/Navbar";
import { Row, Col } from 'react-bootstrap'

function Notfound() {
  return (
    <>
    <Navbar />
    <div className="mt-5 container notFound d-md-flex justify-content-center align-item-center">
       <div> <h1>Error 404</h1></div>
    </div>
    <div className="container notFound d-md-flex justify-content-center align-self-center">
      <div> <p>Not Found 😢</p></div>
    </div>
    <Footer />
    </>
  );
}

export default Notfound;
